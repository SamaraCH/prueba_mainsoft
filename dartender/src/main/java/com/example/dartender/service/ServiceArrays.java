package com.example.dartender.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.dartender.dto.ArraysDTO;
import com.example.dartender.implement.ArraysImp;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/arrays/")
public class ServiceArrays {

	@Autowired
	private ArraysImp arrays;
	
	//get
	@RequestMapping(value = "consultarArrays", method = RequestMethod.GET)
	public ResponseEntity<List<ArraysDTO>> consultarArrays(@RequestParam int id) {
		List<ArraysDTO> lstArrays = arrays.consultaArrays(id);
		return new ResponseEntity<List<ArraysDTO>>(lstArrays, HttpStatus.OK);
	}
	
}
