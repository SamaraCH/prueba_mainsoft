package com.example.dartender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DartenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(DartenderApplication.class, args);
	}

}
