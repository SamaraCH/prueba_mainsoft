package com.example.dartender.dao;

import java.util.List;

import com.example.dartender.dto.ArraysDTO;

public interface ArraysDAO {

	public List<ArraysDTO> consultaArrays(int id);

}
