package com.example.dartender.implement;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.CallableStatementCreator;

import com.example.dartender.dao.ArraysDAO;
import com.example.dartender.dto.ArraysDTO;

@Component
public class ArraysImp implements ArraysDAO {

	@Autowired
	@Qualifier("jdbcMaster")
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<ArraysDTO> consultaArrays(int id) {
		Map<String, Object> resultMap = null;
		List<SqlParameter> parameters = Arrays.asList(new SqlParameter(Types.BIGINT));
		List<ArraysDTO> lstArrays = new ArrayList<ArraysDTO>();
		try {
			resultMap = jdbcTemplate.call(new CallableStatementCreator() {

				@Override
				public CallableStatement createCallableStatement(Connection con) throws SQLException {
					CallableStatement callableStatement = con.prepareCall("{call [dbo].[consultarArraysP](?)}");
					callableStatement.setInt(1, id);

					return callableStatement;
				}
			}, parameters);
		} catch (Exception e) {
			System.out.println("DAO ARRAYS");
			e.printStackTrace();
		}
		System.out.println(resultMap);
		List<Object> lstObjetos = (List<Object>) resultMap.get("#result-set-1");

		for (int i = 0; i < lstObjetos.size(); i++) {
			Map rowMap = (Map) lstObjetos.get(i);
			ArraysDTO asp = new ArraysDTO();
			asp.setId(Integer.parseInt(rowMap.get("id").toString()));
		    asp.setInput_array(rowMap.get("input_array").toString());
			lstArrays.add(asp);
		}
		return lstArrays;
	}

}
