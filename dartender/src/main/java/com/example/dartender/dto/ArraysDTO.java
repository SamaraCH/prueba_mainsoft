package com.example.dartender.dto;

public class ArraysDTO {

	private int id;
	private String input_array;
	
	public ArraysDTO() {
		super();
	}

	public ArraysDTO(int id, String input_array) {
		super();
		this.id = id;
		this.input_array = input_array;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInput_array() {
		return input_array;
	}

	public void setInput_array(String input_array) {
		this.input_array = input_array;
	}

}
